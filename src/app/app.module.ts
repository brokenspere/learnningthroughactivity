import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomMaterialModule } from './core/material.module';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ReplyComponent } from './admin/reply/reply.component';
import { CreateActivityComponent } from './admin/create-activity/create-activity.component';
import { ViewActComponent } from './admin/view-act/view-act.component';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { AppRoutingModule } from './core/app-routing.module';
import { RegisterComponent } from './register/register.component';
import { AdminRoutingModule } from './admin/admin-routing.module';
import { LoginComponent } from './login/login.component';
import { EditInfoComponent } from './student/edit-info/edit-info.component';
import { EnrollActComponent } from './student/enroll-act/enroll-act.component';
import { ProfileComponent } from './student/profile/profile.component';
import { TeacherActivityComponent } from './teacher/teacher-activity/teacher-activity.component';
import { TeacherVerifyEnrollComponent } from './teacher/teacher-verify-enroll/teacher-verify-enroll.component';
import { StudentService } from './service/std-file/student.service';
import { StudentImplService } from './service/std-file/student-impl.service';
import { StudentRoutingModule } from './student/student-rounting.module';
import { TeacherRoutingModule } from './teacher/teacher-routing.module';
import { UserService } from './service/user-file/user.service';
import { RegistService } from './service/regist-file/regist.service';
import { UserImplService } from './service/user-file/user-impl.service';
import { RegistFileService } from './service/regist-file/regist-file.service';
import { ActImplService } from './service/act-file/act-impl.service';
import { NavService } from './service/nav-file/nav.service';
import { NavImplService } from './service/nav-file/nav-impl.service';
import { TeacherImplService } from './service/teacher-file/teacher-impl.service';
import { TeacherService } from './service/teacher-file/teacher.service';
import { ActivityService } from './service/act-file/act.service';
import { EditActDialogComponent } from './teacher/edit-act-dialog/edit-act-dialog.component';
import { ListStdDialogComponent } from './teacher/list-std-dialog/list-std-dialog.component';
import { UpdateActivtyComponent } from './teacher/update-activity/update-activity.component';
import { VerifyStdDialogComponent } from './teacher/verify-std-dialog/verify-std-dialog.component';
import { RejectStdDialogComponent } from './teacher/reject-std-dialog/reject-std-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    ReplyComponent,
    CreateActivityComponent,
    ViewActComponent,
    FileNotFoundComponent,
    RegisterComponent,
    LoginComponent,
    EditInfoComponent,
    EnrollActComponent,
    VerifyStdDialogComponent,
    ProfileComponent,
    TeacherActivityComponent,
    TeacherVerifyEnrollComponent,
    EditActDialogComponent,
    ListStdDialogComponent,
    UpdateActivtyComponent,
    VerifyStdDialogComponent,
    RejectStdDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    TeacherRoutingModule,
    StudentRoutingModule,
    AdminRoutingModule,
    AppRoutingModule
  ],
  providers: [
    { provide: UserService, useClass: UserImplService },
    { provide: StudentService, useClass: StudentImplService },
    { provide: RegistService, useClass: RegistFileService },
    { provide: ActivityService, useClass: ActImplService },
    { provide: TeacherService , useClass: TeacherImplService},
    { provide: NavService , useClass: NavImplService}
  
  ],
  entryComponents: [
    EditActDialogComponent,
    ListStdDialogComponent,
    VerifyStdDialogComponent,
    RejectStdDialogComponent
  ],
  bootstrap: [AppComponent]
  
})
export class AppModule { }
