import { Observable } from "rxjs";
import {Teacher} from "src/app/entity/teacher";

export abstract class TeacherService{
    abstract getTeacher(): Observable<Teacher[]>;
}