import { Injectable } from '@angular/core';
import { TeacherService } from './teacher.service';
import { Observable, of } from 'rxjs';
import {Teacher} from 'src/app/entity/teacher';

@Injectable({
  providedIn: 'root'
})
export class TeacherImplService extends TeacherService {
  constructor() {
    super()
  }
  getTeacher(): Observable<Teacher[]> {
    return of(this.teachers);
  }

  teachers: Teacher[] = [
    {
      "teacherId": "T-001",
      "name": "Dr. Pattama",
      "surname": "Longani",
      "username": "teacher",
      "password": "teacher"
    },
    {
      "teacherId": "T-002",
      "name": "Dr. Chartchai",
      "surname": "Doungsa-ard",
      "username": "teacher1",
      "password": "teacher1"
    }

  ];

}
