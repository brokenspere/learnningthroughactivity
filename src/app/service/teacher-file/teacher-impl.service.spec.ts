import { TestBed, inject } from '@angular/core/testing';

import { TeacherImplService } from './teacher-impl.service';

describe('TeacherImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeacherImplService]
    });
  });

  it('should be created', inject([TeacherImplService], (service: TeacherImplService) => {
    expect(service).toBeTruthy();
  }));
});
