import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from '../../entity/user';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class UserImplService extends UserService {

  constructor() {
    super();
  }

  getUsers(): Observable<User[]> {
    return of(this.users);
  }

  // Set the method do in register class
  setnewUser(user: User): Observable<User> {
    this.users.push(user);
    return of(user);
  }

  setcurrentUser(username: string): Observable<User> {
    for (const user of this.users) {
      if (user.username == username) {
        this.userLogin = {
          "username": user.username,
          "password": user.password,
          "role": user.role
        }
      }
    }
    return of(this.userLogin);
  }

  getcurrentUser(): Observable<User> {
    return of(this.userLogin);
  }

  updateUser(userInfo: User): Observable<User> {
    for (const user of this.users) {
      if (this.userLogin.username == user.username) {
        let index = this.users.indexOf(this.userLogin);
        this.users[index] = userInfo;
      }
    }
    return of(userInfo);
  }

  deleteUser(user: User): Observable<User[]> {
    let index = this.users.indexOf(user);
    this.users.splice(index, 1);
    return of(this.users);
  }



  userLogin: User;
  users: User[] = [
    {
      "username": "sun@gmail.com",
      "password": "123456",
      "role": "student"
    },
    {
      "username": "student1",
      "password": "student1",
      "role": "student"
    },
    {
      "username": "teacher",
      "password": "teacher",
      "role": "teacher"
    },
    {
      "username": "admin",
      "password": "admin",
      "role": "admin"
    },
    {
      "username": "teacher1",
      "password": "teacher1",
      "role": "teacher"
    }
  ];

}