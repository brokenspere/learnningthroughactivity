import { TestBed, inject } from '@angular/core/testing';

import { UserImplService } from './user-impl.service';

describe('UserImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserImplService]
    });
  });

  it('should be created', inject([UserImplService], (service: UserImplService) => {
    expect(service).toBeTruthy();
  }));
});
