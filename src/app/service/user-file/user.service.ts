import { Observable } from "rxjs";
import { User } from "src/app/entity/user";

export abstract class UserService {
    abstract getUsers(): Observable<User[]>;
    abstract setnewUser(user: User): Observable<User>;
    abstract setcurrentUser(username: string): Observable<User>;
    abstract updateUser(user: User): Observable<User>;
    abstract getcurrentUser(): Observable<User>;
    abstract deleteUser(user: User): Observable<User[]>;

}