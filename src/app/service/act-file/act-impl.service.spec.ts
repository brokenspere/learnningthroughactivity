import { TestBed, inject } from '@angular/core/testing';

import { ActImplService } from './act-impl.service';

describe('ActImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActImplService]
    });
  });

  it('should be created', inject([ActImplService], (service: ActImplService) => {
    expect(service).toBeTruthy();
  }));
});
