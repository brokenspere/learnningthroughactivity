import { Injectable } from '@angular/core';
import { ActivityService } from './act.service';
import { Observable, of } from 'rxjs';
import { Activity } from 'src/app/entity/activity';

@Injectable({
  providedIn: 'root'
})
export class ActImplService extends ActivityService {

  constructor() {
    super()
  }
 
  getActivityList(): Observable<Activity[]> {
    return of(this.activity);
  }

  setActivityListId(id: string): Observable<Activity> {
    for (const act of this.activity) {
      if (act.activityId == id) {
        this.findActivity = act;
        return of(this.findActivity);
      }
    }
    return null;
  }

  getActivityListId(): Observable<Activity> {
    return of(this.findActivity);
  }
 
  findActivity: Activity;
  activity: Activity[] = [
    {
      "position": 1,
      "activityId": "ACT-001",
      "name": "Moojum",
      "location": "Moo 2 chun",
      "description": "eat moojum tee moo 2 chun",
      "period": new Date('10/10/2018'),
      "start": new Date('10/15/2018'),
      "end": new Date('10/17/2018'),
      "advisor": "123456",
      "image": "assets/act-img/moojum.jpg",
      "studentIdEnroll": ["SE-001", "SE-002"]

    },
    {
      "position": 2,
      "activityId": "ACT-002",
      "name": "Meet lung tu",
      "location": "Tum neab ruttabarn",
      "description": "2 shot with lung tu",
      "period": new Date('12/8/2018'),
      "start": new Date('8/11/2018'),
      "end": new Date('9/11/2018'),
      "advisor": "AjTo",
      "image": "assets/act-img/tuExercise.jpg",
      "studentIdEnroll": ["SE-001"]
    },

    {
      "position": 3,
      "activityId": "ACT-003",
      "name": "Meet lung pravit",
      "location": "Tum neab ruttabarn",
      "description": "2 shot with lung pravit",
      "period": new Date('11/8/2018'),
      "start": new Date('11/5/2018'),
      "end": new Date('11/4/2018'),
      "advisor": "AjSun",
      "image": "assets/act-img/pravit.jpg",
      "studentIdEnroll": ["SE-002"]

    },

  ]


}
