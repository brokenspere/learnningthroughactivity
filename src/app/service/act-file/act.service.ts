import { Activity } from "src/app/entity/activity";
import { Observable } from "rxjs";

export abstract class ActivityService{
    abstract getActivityList(): Observable<Activity[]>;
    abstract setActivityListId(id: string) : Observable<Activity>;
    abstract getActivityListId() : Observable<Activity>;
}