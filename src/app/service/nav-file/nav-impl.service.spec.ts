import { TestBed, inject } from '@angular/core/testing';

import { NavImplService } from './nav-impl.service';

describe('NavImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NavImplService]
    });
  });

  it('should be created', inject([NavImplService], (service: NavImplService) => {
    expect(service).toBeTruthy();
  }));
});
