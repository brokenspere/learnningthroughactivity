import { Injectable } from '@angular/core';
import { NavService } from './nav.service';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavImplService extends NavService{
  constructor() {
    super()
   }

  setActive(): Observable<boolean> {
    this.isActive = true;
    return of(this.isActive)
  }
  
  isActive: boolean;
}
