import { Observable } from "rxjs";

export abstract class NavService{
    abstract setActive(): Observable<boolean>;
}