import { TestBed, inject } from '@angular/core/testing';

import { StudentImplService } from './student-impl.service';

describe('StudentImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentImplService]
    });
  });

  it('should be created', inject([StudentImplService], (service: StudentImplService) => {
    expect(service).toBeTruthy();
  }));
});
