import { Injectable } from '@angular/core';
import { StudentService } from './student.service';
import { Observable, of } from 'rxjs';
import { Student } from 'src/app/entity/student';

@Injectable({
  providedIn: 'root'
})
export class StudentImplService extends StudentService {

  getStudents(): Observable<Student[]> {
    return of(this.students);
  }

  setnewStudent(student: Student): Observable<Student> {
    this.students.push(student);
    return of(student);
  }

  updateInfo(studentInfo: Student): Observable<Student> {
    for (const student of this.students) {
      if (studentInfo.studentId == student.studentId) {
        let index = this.students.indexOf(student);
        this.students[index] = studentInfo;
      }
    }
    return of(studentInfo);
  }

  getcurrentStudent(): Observable<Student> {
    return of(this.studentLogin);
  }

  setcurrentStudent(username: string): Observable<Student> {
    for (const student of this.students) {
      if (student.username == username) {
        console.log('set current student: ' + student);
        this.studentLogin = {
          "studentId": student.studentId,
          "name": student.name,
          "surname": student.surname,
          "dateofbirth": student.dateofbirth,
          "image": student.image,
          "username": student.username,
          "password": student.password
        }
        console.log(this.studentLogin);
      }
    }
    return of(this.studentLogin);
  }

  deleteStudent(student: Student): Observable<Student[]> {
    let index = this.students.indexOf(student);
    this.students.splice(index, 1);
    return of(this.students);
  }


  studentLogin: Student;
  students: Student[] = [
    {
      "studentId": "SE-001",
      "name": "Pumipat",
      "surname": "Tippayawong",
      "dateofbirth": "21-Nov-1997",
      "image": "assets/student-img/sun.jpg",
      "username": "sun@gmail.com",
      "password": "123456"
    },
    {
      "studentId": "SE-002",
      "name": "Nattapat",
      "surname": "Tamtrakool",
      "dateofbirth": "1-Jan-1997",
      "image": "assets/student-img/dan.jpg",
      "username": "student1",
      "password": "student1"
    }
  ];
}
