import { Observable } from "rxjs";
import { Student } from "src/app/entity/student";

export abstract class StudentService {
    abstract getStudents(): Observable<Student[]>;
    abstract setcurrentStudent(username: string): Observable<Student>;
    abstract getcurrentStudent(): Observable<Student>;
    abstract setnewStudent(student: Student): Observable<Student>;
    abstract updateInfo(student: Student): Observable<Student>;
    abstract deleteStudent(student: Student): Observable<Student[]>;
}