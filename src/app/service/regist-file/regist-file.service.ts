import { Injectable } from '@angular/core';
import {Student} from 'src/app/entity/student';
import { Observable, of } from 'rxjs';
import { RegistService } from './regist.service';

@Injectable({
  providedIn: 'root'
})
export class RegistFileService extends RegistService {
 
  constructor() {
    super()
  }
  getRegistlist(): Observable<Student[]> {
    return of(this.registStudents);
  }

  sendnewStudent(student: Student): Observable<Student> {
    this.registStudents.push(student);
    return of(student);
  }

  deletenewStudent(student: Student): Observable<Student[]> {
    let index = this.registStudents.indexOf(student);
    this.registStudents.splice(index,1);
    return of(this.registStudents);
  }
 

  registStudents: Student[] = [
    {
      "studentId": "SE-003",
      "name": "Nattapong",
      "surname": "Thanakunkan",
      "dateofbirth": "2-Jan-1997",
      "image": "assets/regist-img/kim.jpg",
      "username": "student2",
      "password": "student2"
    },
    {
      "studentId": "SE-004",
      "name": "Saksit",
      "surname": "Namhueng",
      "dateofbirth": "3-Jan-1997",
      "image": "assets/regist-img/nieb.jpg",
      "username": "student3",
      "password": "student3"
    },
  {
    "studentId": "SE-005",
      "name": "Pukhom",
      "surname": "BNK48",
      "dateofbirth": "3-Jan-1998",
      "image": "assets/regist-img/pukhom.jpg",
      "username": "student4",
      "password": "student4"
  }
  ]
}
