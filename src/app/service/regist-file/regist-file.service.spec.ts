import { TestBed, inject } from '@angular/core/testing';

import { RegistFileService } from './regist-file.service';

describe('RegistFileService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RegistFileService]
    });
  });

  it('should be created', inject([RegistFileService], (service: RegistFileService) => {
    expect(service).toBeTruthy();
  }));
});
