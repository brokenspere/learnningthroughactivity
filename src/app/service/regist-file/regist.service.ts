import { Observable } from "rxjs";
import {Student} from "src/app/entity/student";

export abstract class RegistService{
    abstract getRegistlist(): Observable<Student[]>;
    abstract sendnewStudent(student: Student): Observable<Student>;
    abstract deletenewStudent(student: Student): Observable<Student[]>;
}