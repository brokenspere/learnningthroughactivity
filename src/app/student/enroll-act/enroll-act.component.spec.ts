import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollActComponent } from './enroll-act.component';

describe('EnrollActComponent', () => {
  let component: EnrollActComponent;
  let fixture: ComponentFixture<EnrollActComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrollActComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollActComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
