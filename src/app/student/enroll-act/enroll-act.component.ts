import { Component, OnInit } from '@angular/core';
import { ActivityService } from 'src/app/service/act-file/act.service';
import {Activity} from 'src/app/entity/activity';
import { TeacherService } from 'src/app/service/teacher-file/teacher.service';
import {Teacher} from 'src/app/entity/teacher';

@Component({
  selector: 'app-enroll-act',
  templateUrl: './enroll-act.component.html',
  styleUrls: ['./enroll-act.component.css']
})
export class EnrollActComponent implements OnInit {
  activities: Activity[];
  teachers: Teacher[];

  constructor(private activityService: ActivityService, private teacherService: TeacherService) { }

  ngOnInit() {
    
  }

}