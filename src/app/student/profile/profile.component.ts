import { Component, OnInit } from '@angular/core';
import { StudentService } from 'src/app/service/std-file/student.service';
import { Student } from 'src/app/entity/student';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private studentService: StudentService) { }
  ngOnInit() {
    this.studentService.getcurrentStudent()
      .subscribe(student => this.loginStudent = student)
  }

  loginStudent: Student;

}
