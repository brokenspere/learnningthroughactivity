import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/service/user-file/user.service';
import { StudentService } from 'src/app/service/std-file/student.service';

import { Router } from '@angular/router';
import { User } from 'src/app/entity/user';
import { Student } from '../../entity/student';

@Component({
  selector: 'app-edit-info',
  templateUrl: './edit-info.component.html',
  styleUrls: ['./edit-info.component.css']
})
export class EditInfoComponent implements OnInit {

  validation_messages = {

    'Name': [
      { type: 'required', message: 'the first name is required' }
    ],
    'Surname': [
      { type: 'required', message: 'the last name is required' }
    ],
    'birthday': [
      { type: 'required', message: 'Please insert your birthday' }
    ],
    'username': [
      { type: 'required', message: 'Username is required' },
      { type: 'email', message: 'Username must be an email' },

    ],
    'password': [
      { type: 'required', message: 'Password is required' },
      { type: 'minlength', message: 'Password must be at least 5 characters long' },
      { type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number' }
    ],
    'image': [{ type: 'required', message: 'Image is required' }]
  };

  constructor(private userService: UserService,
    private studentService: StudentService,
    private router: Router
  ) { }

  loginUser: User;
  loginStudent: Student;
  fb: FormBuilder = new FormBuilder();
  sf: FormGroup;
  model: Student = new Student();
  studentId: string;
  name: string;
  surname: string;
  bod: string;
  image: string;
  username: string;
  password: string;

  ngOnInit() {
    this.sf = this.fb.group({
      name: [null, Validators.required],
      surname: [null, Validators.required],
      bod: [null, Validators.required],
      username: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(5)])],
      image: [null, Validators.required]
    });

    this.studentService.getcurrentStudent()
      .subscribe(student => this.loginStudent = student);
    this.userService.getcurrentUser()
      .subscribe(user => this.loginUser = user);

  }
  // , Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')

  onSubmit(): void {
    let index = this.loginStudent.studentId;

    this.model = {
      "studentId": index,
      "name": this.name,
      "surname": this.surname,
      "dateofbirth": this.bod,
      "image": this.image,
      "username": this.username,
      "password": this.password
    }

    console.log(this.model);
    let user: User = {
      "username": this.loginUser.username,
      "password": this.loginUser.password,
      "role": "student"
    };

    this.studentService.updateInfo(this.model)
    console.log(this.loginUser + ' - login [user]');
    this.studentService.setnewStudent(this.model)
      .subscribe();
    this.studentService.setcurrentStudent(this.model.username)
      .subscribe();
    this.userService.updateUser(user)
      .subscribe();
    this.userService.setcurrentUser(this.model.username)
      .subscribe();
    this.router.navigate(['profile']);

  }
}

