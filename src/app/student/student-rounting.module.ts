import { Routes, RouterModule } from "@angular/router";
import { EditInfoComponent } from "./edit-info/edit-info.component";
import { ProfileComponent } from "./profile/profile.component";
import { EnrollActComponent } from "./enroll-act/enroll-act.component";
import { NgModule } from "@angular/core";

const studentRoutes: Routes = [
    { path: 'edit-info', component: EditInfoComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'enroll-act', component: EnrollActComponent}
]

@NgModule({
    imports: [
        RouterModule.forRoot(studentRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class StudentRoutingModule { } 