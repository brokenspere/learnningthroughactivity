import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ReplyComponent } from "./reply/reply.component";
import { CreateActivityComponent } from "./create-activity/create-activity.component";
import { ViewActComponent } from "./view-act/view-act.component";

const adminRoutes: Routes = [
    { path: 'reply', component: ReplyComponent },
    { path: 'create-act' , component: CreateActivityComponent},
    { path: 'view-act', component: ViewActComponent },
    
]

@NgModule({
    imports: [
        RouterModule.forRoot(adminRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class AdminRoutingModule { } 