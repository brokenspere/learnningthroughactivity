import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewActComponent } from './view-act.component';

describe('ViewActComponent', () => {
  let component: ViewActComponent;
  let fixture: ComponentFixture<ViewActComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewActComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewActComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
