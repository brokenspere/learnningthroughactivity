import { Component, OnInit } from '@angular/core';
import { Activity } from 'src/app/entity/activity';
import { ActivityService } from 'src/app/service/act-file/act.service';

@Component({
  selector: 'app-view-act',
  templateUrl: './view-act.component.html',
  styleUrls: ['./view-act.component.css']
})
export class ViewActComponent implements OnInit {

  activities: Activity[];

  constructor(private activityService: ActivityService) { }

  ngOnInit() {
    this.activityService.getActivityList()
      .subscribe(activities => this.activities = activities);
  }
}
