import { Component, OnInit } from '@angular/core';
import { FormGroup , Validators , FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-create-activity',
  templateUrl: './create-activity.component.html',
  styleUrls: ['./create-activity.component.css']
})
export class CreateActivityComponent implements OnInit {

  fb: FormBuilder = new FormBuilder();
  sf: FormGroup;
  validation_messages = {
    'name':[
      {type: 'required', message: 'the name is required'}
    ],
    'location':[
      { type: 'required',message:'the location is required'}
    ],
    'description':[
      { type: 'required',message:'the description is required'}
    ],
    'host':[
      { type: 'required',message:'activity host is required'}
    ]


  }

  ngOnInit(): void {
    this.sf = this.fb.group({
      name: [null ,Validators.required],
      location: [null ,Validators.required],
      description: [null ,Validators.required],
      host:  [null ,Validators.required]

    });
      

  }
  
  onSubmit(){

  }


}
