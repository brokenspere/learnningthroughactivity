import { Component, OnInit } from '@angular/core';
import { Student } from 'src/app/entity/student';
import { RegistService } from 'src/app/service/regist-file/regist.service';
import { StudentService } from 'src/app/service/std-file/student.service';
import { UserService } from 'src/app/service/user-file/user.service';
import { User } from 'src/app/entity/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reply',
  templateUrl: './reply.component.html',
  styleUrls: ['./reply.component.css']
})
export class ReplyComponent implements OnInit {
  newstudents: Student[];

  constructor(private regisService: RegistService,
    private studentService: StudentService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.regisService.getRegistlist()
      .subscribe(newstudents => this.newstudents = newstudents);
  }

  confirm(selectStudent: Student) {

    let user: User = {
      "username": selectStudent.username,
      "password": selectStudent.password,
      "role": "student"
    };
    console.log('old regist' + '--->' + this.newstudents);
    this.studentService.setnewStudent(selectStudent)
      .subscribe((selectStudent) => {
        console.log('Add new student' + '--->' + selectStudent);
        this.userService.setnewUser(user)
          .subscribe((user) => {
            console.log('Add new user' + '--->' + user);
            this.regisService.deletenewStudent(selectStudent)
              .subscribe(newstudents => this.newstudents = newstudents);
            console.log('new regist' + '--->' + this.newstudents);
          });
      });
  };

}
