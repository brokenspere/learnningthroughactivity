import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FileNotFoundComponent } from "../shared/file-not-found/file-not-found.component";
import { LoginComponent } from "../login/login.component";
import { RegisterComponent } from "../register/register.component";

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    { path: 'login', component: LoginComponent },
    { path: 'regist', component: RegisterComponent },
    { path: '**', component: FileNotFoundComponent }
]

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})

export class AppRoutingModule { } 