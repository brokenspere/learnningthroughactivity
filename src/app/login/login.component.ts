import { Component, OnInit } from '@angular/core';
import { User } from '../entity/user';
import { UserService } from '../service/user-file/user.service';
import { Router } from '@angular/router';
import { StudentService } from '../service/std-file/student.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  users: User[];
  username: string;
  password: string;

  constructor(private userService: UserService,
    private router: Router,
    private studentService: StudentService) { }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe(users => this.users = users);
  }

  login(): void {
    let isValid = false;
    let thisRole = null;
    if (Array.isArray(this.users)) {
      for (const user of this.users) {
        if (this.username == user.username && this.password == user.password) {
          thisRole = user.role;
          isValid = true;
          this.userService.setcurrentUser(this.username)
            .subscribe();
        }
      }
      if (isValid) {
        console.log(this.username + ' --> setcurrentUser');
        this.userService.setcurrentUser(this.username)
          .subscribe();
        if (thisRole == 'admin') {
          this.router.navigate(['reply']);
          
        } else if (thisRole == 'student') {
          console.log(this.username + ' --> setcurrentStudent');
          this.studentService.setcurrentStudent(this.username)
            .subscribe();
          this.router.navigate(['profile']);

        } else if (thisRole == 'teacher') {

        } else {
          alert("Incorrect in role!");
        }
      } else {
        alert("Incorrect something!");
      }
    } else {
      return null;
    }
  }


}
