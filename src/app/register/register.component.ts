import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import {Student} from '../entity/student';
import { RegistService } from '../service/regist-file/regist.service';
import { Router } from '@angular/router';
import { UserService } from '../service/user-file/user.service';
import {User} from '../entity/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  fb: FormBuilder = new FormBuilder();
  users: User[];
  model: Student = new Student();
  sf: FormGroup;
  validation_messages = {

    'name': [
      { type: 'required', message: 'the first name is required' }
    ],
    'surname': [
      { type: 'required', message: 'the last name is required' }
    ],
    'studentId': [
      { type: 'required', message: 'the StudentId is required' },
      { type: 'maxlength', message: 'StudentId is too long' }
    ],
    'birthday': [],
    'username': [
      { type: 'required', message: 'Username is required' },
      { type: 'email', message: 'Username must be an email' },

    ],
    'password': [
      { type: 'required', message: 'Password is required' },
      { type: 'minlength', message: 'Password must be at least 5 characters long' },
      // { type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number' }
    ],
    'image': []
  };


  constructor(private registService: RegistService,
    private router: Router,
    private userService: UserService) { }

  ngOnInit(): void {
    this.sf = this.fb.group({
      studentId: [null, Validators.compose([Validators.required, Validators.maxLength(9)])],
      name: [null, Validators.required],
      surname: [null, Validators.required],
      birthday: [null],
      username: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(5)])],
      image: [null]
    });
    // Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')

    this.userService.getUsers()
      .subscribe(users => this.users = users);
  }

  onSubmit() {
    this.model = this.sf.value;
    console.log(this.model);
    let hasAccount: boolean = false;
    // verify account
    
    for(const user of this.users){
      if(user.username == this.model.username){
        hasAccount = true;
      }
    }

    if(!hasAccount){
      this.registService.sendnewStudent(this.model)
      .subscribe((student) => {
        this.router.navigate(['login']);
      }, (error) => {
        console.log(error)
        alert('could not send value');
      });
    }else{
      alert('Already has this username');
    }



    
  }

}
