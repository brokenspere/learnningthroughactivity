export class Teacher {
    teacherId: string;
    name: string;
    surname: string;
    username: string;
    password: string;
}
