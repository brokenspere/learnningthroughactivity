export class Student {
    studentId: string;
    name: string;
    surname: string;
    dateofbirth: string;
    image: string;
    username: string;
    password: string;
}
