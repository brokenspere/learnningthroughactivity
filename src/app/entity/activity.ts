export class Activity {
  position: number;
  activityId: string;
  name: string;
  location: string;
  description: string;
  period: Date;
  start: Date;
  end: Date;
  advisor: string;
  image:string;
  studentIdEnroll: string[];
}
