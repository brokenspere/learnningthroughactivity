import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { Student } from '../../entity/student';
import { StudentService } from '../../service/std-file/student.service';
import { VerifyStdDialogComponent } from '../verify-std-dialog/verify-std-dialog.component';
import { RejectStdDialogComponent } from '../reject-std-dialog/reject-std-dialog.component';

@Component({
  selector: 'app-teacher-verify-enroll',
  templateUrl: './teacher-verify-enroll.component.html',
  styleUrls: ['./teacher-verify-enroll.component.css']
})
export class TeacherVerifyEnrollComponent implements OnInit {

  ngOnInit(): void {
    this.studentservice.getStudents().subscribe(students=>{
      this.dataSource = new MatTableDataSource(students);
    })
  }

  displayedColumns: string[] = ['studentId', 'name','surname','dateofbirth','btn'];
  dataSource : MatTableDataSource<Student>;

  constructor(public dialog: MatDialog, private studentservice: StudentService) {
   
  }

  verifyDialog(): void {
    const dialogRef = this.dialog.open(VerifyStdDialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  rejectDialog(): void {
    const dialogRef = this.dialog.open(RejectStdDialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
