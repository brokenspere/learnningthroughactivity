import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherVerifyEnrollComponent } from './teacher-verify-enroll.component';

describe('TeacherVerifyEnrollComponent', () => {
  let component: TeacherVerifyEnrollComponent;
  let fixture: ComponentFixture<TeacherVerifyEnrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherVerifyEnrollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherVerifyEnrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
