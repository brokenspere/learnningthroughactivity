import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectStdDialogComponent } from './reject-std-dialog.component';

describe('RejectStdDialogComponent', () => {
  let component: RejectStdDialogComponent;
  let fixture: ComponentFixture<RejectStdDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectStdDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectStdDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
