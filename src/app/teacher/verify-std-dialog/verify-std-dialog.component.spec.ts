import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyStdDialogComponent } from './verify-std-dialog.component';

describe('VerifyStdDialogComponent', () => {
  let component: VerifyStdDialogComponent;
  let fixture: ComponentFixture<VerifyStdDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyStdDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyStdDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
