import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-activity',
  templateUrl: './update-activity.component.html',
  styleUrls: ['./update-activity.component.css']
})
export class UpdateActivtyComponent implements OnInit {

  fb: FormBuilder = new FormBuilder();
  sf: FormGroup;
  validation_messages = {
    'name': [],
    'location': [
      { type: 'required', message: 'the location is required' }
    ],
    'description': [
      { type: 'required', message: 'the description is required' }
    ],
    'period': [

    ],
    'start': [
      { type: 'required', message: 'the start time is required' }
    ],
    'end': [
      { type: 'required', message: 'the end time is required' }
    ],
    'host': []
  }

  ngOnInit(): void {
    this.sf = this.fb.group({
      name: [],
      location: [null, Validators.required],
      description: [null, Validators.required],
      start: [null, Validators.required],
      end: [null, Validators.required],
      host: []
    });
  }

  constructor(private router: Router) {

  }

  onSubmit() {
    this.router.navigate[("teacher-act")];
  }
}
