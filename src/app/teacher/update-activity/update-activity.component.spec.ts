import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateActivtyComponent } from './update-activity.component';

describe('UpdateActivityComponent', () => {
  let component: UpdateActivtyComponent;
  let fixture: ComponentFixture<UpdateActivtyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateActivtyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateActivtyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
