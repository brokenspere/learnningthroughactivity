import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-edit-act-dialog',
  templateUrl: './edit-act-dialog.component.html',
  styleUrls: ['./edit-act-dialog.component.css']
})
export class EditActDialogComponent implements OnInit {

  position: number;
    activityId: string;
    name: string;
  location: string;
  description: string;
  period:Date;
  start:Date;
  end:Date;
  host:string;

constructor(public dialog: MatDialog) {}

ngOnInit(){

  }

}
