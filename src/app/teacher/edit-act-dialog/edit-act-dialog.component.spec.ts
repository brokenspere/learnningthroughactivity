import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditActDialogComponent } from './edit-act-dialog.component';

describe('EditActDialogComponent', () => {
  let component: EditActDialogComponent;
  let fixture: ComponentFixture<EditActDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditActDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditActDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
