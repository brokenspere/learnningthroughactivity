import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListStdDialogComponent } from './list-std-dialog.component';

describe('ListStdDialogComponent', () => {
  let component: ListStdDialogComponent;
  let fixture: ComponentFixture<ListStdDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListStdDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListStdDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
