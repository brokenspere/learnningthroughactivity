import { Component, OnInit } from '@angular/core';
import { ActivityService } from 'src/app/service/act-file/act.service';
import { Activity } from 'src/app/entity/activity';
import { StudentService } from 'src/app/service/std-file/student.service';
import { Student } from 'src/app/entity/student';

@Component({
  selector: 'app-list-std-dialog',
  templateUrl: './list-std-dialog.component.html',
  styleUrls: ['./list-std-dialog.component.css']
})
export class ListStdDialogComponent implements OnInit {

  constructor(private activityService: ActivityService,
    private studentService: StudentService) { }

  selectActId: Activity;
  students: Student[];
  studentListinActivity: Student[] = [];

  ngOnInit() {
    console.log(this.studentListinActivity)
    this.studentService.getStudents()
      .subscribe(students => this.students = students);
    this.activityService.getActivityListId()
      .subscribe(act => this.selectActId = act);
      
    // for ( const studentInAct of this.selectAct.studentIdEnroll){
    //     for( const student of this.students ){
    //         if(studentInAct == student.studentId){
    //             this.studentListinActivity.push(student);
    //         }
    //     }
    // }
  }

}
