import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { Activity } from '../../entity/activity';
import { ActivityService } from '../../service/act-file/act.service';
import { EditActDialogComponent } from '../edit-act-dialog/edit-act-dialog.component';
import { ListStdDialogComponent } from '../list-std-dialog/list-std-dialog.component';

@Component({
  selector: 'app-teacher-activity',
  templateUrl: './teacher-activity.component.html',
  styleUrls: ['./teacher-activity.component.css']
})
export class TeacherActivityComponent implements OnInit {

  ngOnInit(): void {
    this.activityService.getActivityList().subscribe(activities => {
      this.dataSource = new MatTableDataSource(activities);
    })
  }

  displayedColumns: string[] = ['position', 'activityId', 'name', 'location', 'description', 'period', 'start', 'end', 'host', 'editbtn'];
  dataSource: MatTableDataSource<Activity>;

  constructor(public dialog: MatDialog, private activityService: ActivityService) {

  }

  editDialog(): void {
    const dialogRef = this.dialog.open(EditActDialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  listDialog(id: string): void {
    console.log(id)
    this.activityService.setActivityListId(id)
      .subscribe();
    const dialogRef = this.dialog.open(ListStdDialogComponent, {
      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}

