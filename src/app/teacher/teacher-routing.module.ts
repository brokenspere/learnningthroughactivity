import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TeacherActivityComponent } from "./teacher-activity/teacher-activity.component";
import { TeacherVerifyEnrollComponent } from "./teacher-verify-enroll/teacher-verify-enroll.component";

const teacherRoutes: Routes = [
    { path: 'teacher-act', component: TeacherActivityComponent },
    { path: 'verify-enroll' , component: TeacherVerifyEnrollComponent}
]

@NgModule({
    imports: [
        RouterModule.forRoot(teacherRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class TeacherRoutingModule { } 